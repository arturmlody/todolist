﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using ToDoBackend.Logic.UserManagement;
using ToDoBackend.RestApi.Models;

namespace ToDoBackend.RestApi.Controllers
{
    [Route("EditAccount")]
    [Authorize]
    public class EditAccountController : Controller
    {
        private readonly IUserAccManager accManager;

        public EditAccountController(IUserAccManager accManager)
        {
            this.accManager = accManager;
        }

        [HttpGet]
        public async Task <string> GetUserData()
        {
            string id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var creds = accManager.GetUser(id);
            return await creds;
        }

        [HttpPut]
        public async Task<ActionResult> ChangerUserData([FromBody]AccountEditRequest editRequest) 
        {
            string id = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await accManager.ChangePersonalData(id, editRequest.Name, editRequest.Surname);
            var creds = accManager.GetUser(id);
            if (result)
            {
                return NoContent();
            }
            return BadRequest();

        }
    }
}
