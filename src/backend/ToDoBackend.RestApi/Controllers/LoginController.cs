﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using ToDoBackend.Logic.UserManagement;
using ToDoBackend.RestApi.Models;

namespace ToDoBackend.RestApi.Controllers
{
    
    public class LoginController : Controller
    {
        private readonly IUserLoginManager loginManager;

        public LoginController(IUserLoginManager loginManager)
        {
            this.loginManager = loginManager ?? throw new ArgumentNullException(nameof(loginManager));
        }

        [Route("Login")]
        [HttpPost]
        public async Task <ActionResult> Login([FromBody]LoginRequest loginRequest) 
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await loginManager.TrySignIn(loginRequest.Username, loginRequest.Password);
            if (result)
            {
                return NoContent();
            }
            else
            {
                ModelState.AddModelError("", "Invalid name or password");
                return BadRequest(ModelState);
            }
        }

        [Route("Logout")]
        [HttpPost]
        public async Task <ActionResult> Logout()
        {
            var result = await loginManager.TrySignOut();
            if (result)
            {
                return NoContent();
            }
            else
            {
                ModelState.AddModelError("", "Failed to logout");
                return BadRequest(ModelState);
            }
        }
    }
}
