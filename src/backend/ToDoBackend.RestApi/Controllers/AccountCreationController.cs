﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ToDoBackend.Logic.UserManagement;
using ToDoBackend.RestApi.Models;

namespace ToDoBackend.RestApi.Controllers
{
    [Route("CreateAccount")]
    public class AccountCreationController : Controller
    {
        private readonly IUserCreatorManager userCreator;

        public AccountCreationController(IUserCreatorManager userCreator)
        {
            this.userCreator = userCreator;
        }

        [HttpPost]
        public async Task <ActionResult> Create([FromBody] AccountCreateRequest createRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await userCreator.TryCreate(createRequest.UserName, createRequest.Password, 
                createRequest.Name, createRequest.Surname, createRequest.Email);
            if (result.Succeeded)
            {
                return NoContent();
            }
            else
            {
                foreach (IdentityError err in result.Errors)
                {
                    ModelState.AddModelError("", err.Description);
                }
                return BadRequest(ModelState);
            }
        }
    }
}
