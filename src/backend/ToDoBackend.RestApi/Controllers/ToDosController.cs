﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using ToDoBackend.Logic.ToDoList;
using ToDoBackend.RestApi.Models;

namespace ToDoBackend.RestApi.Controllers
{
    [Route("Listing/{listingid}/Todos")]
    [Authorize]
    public class ToDosController : Controller
    {
        private IToDosManager toDosManager;
        private IUserChecker checker;

        public ToDosController(IToDosManager toDosManager, IUserChecker checker)
        {
            this.toDosManager = toDosManager;
            this.checker = checker;
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] ToDosRequest toDosRequest, [FromRoute] long listingid, CancellationToken token = default)
        {
            var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null)
            {
                return BadRequest();
            }
            var checkResult = await checker.CheckUser(listingid, userId);
            if (!checkResult)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var toDos = await toDosManager.Create(listingid, toDosRequest.Content, toDosRequest.Localization, token);
            return Ok(toDos.Id);
        }

        [Route("{id}")]
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] ToDosRequest toDosRequest, [FromRoute] long listingid, [FromRoute] long? id, CancellationToken token = default)
        {
            var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null)
            {
                return BadRequest();
            }

            var checkResult = await checker.CheckUser(listingid, userId);

            if (!checkResult)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await toDosManager.Update(id, toDosRequest.Content, toDosRequest.Localization, token);
            if (!result)
            {
                return BadRequest(result);
            }
            return NoContent();
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<ActionResult> Delete([FromRoute] long? id, [FromRoute] long listingid, CancellationToken token = default)
        {
            var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null)
            {
                return BadRequest();
            }

            var checkResult = await checker.CheckUser(listingid, userId);

            if (!checkResult)
            {
                return NotFound();
            }

            if (id == null)
            {
                return BadRequest(id);
            }
            await toDosManager.Delete(id, token);
            return Ok(id);
        }

        [HttpGet]
        public async Task<List<ToDosResponse>> ReadAllListing([FromRoute] long listingid, CancellationToken token = default)
        {
            var listing = await toDosManager.Read(listingid, token);

            var response = listing.Select(a => new ToDosResponse() { Id = a.Id, Content = a.Content, Localization = a.Localization.ToString() }).ToList();

            return response;
        }
    }
}
