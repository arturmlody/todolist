﻿namespace ToDoBackend.RestApi.Models
{
    public class ListingResponse
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Deadline { get; set; }
    }
}
