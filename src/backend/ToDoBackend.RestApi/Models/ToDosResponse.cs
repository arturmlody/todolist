﻿namespace ToDoBackend.RestApi.Models
{
    public class ToDosResponse
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public string Localization { get; set; }
    }
}
