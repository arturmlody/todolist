﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ToDoBackend.RestApi.Models
{
    public class ToDosRequest
    {
        [BindProperty]
        [Required]
        [StringLength(40, MinimumLength = 3)]
        public string Content { get; set; }

        [BindProperty]
        [Required]
        [StringLength(40, MinimumLength = 3)]
        public string Localization { get; set; }
    }
}
