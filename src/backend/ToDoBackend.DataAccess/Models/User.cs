﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace ToDoBackend.DataAccess.Models
{
    public class User : IdentityUser
    {
        [PersonalData]
        public string Name { get; set; }
        [PersonalData]
        public string Surname { get; set; }
        public virtual ICollection<ToDoListing> Listings { get; set; } = new HashSet<ToDoListing>();
    }
}
