﻿namespace ToDoBackend.DataAccess.Models
{
    public class ToDos
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public long? ListingId { get; set; }
        public ToDoListing Listing { get; set; }
        public string Localization { get; set; }
    }
}
