﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ToDoBackend.DataAccess.Models
{
    public interface IListingRepository
    {
        IQueryable<ToDoListing> Listings { get; }

        Task SaveListing(ToDoListing l, CancellationToken token = default);
        Task CreateListing(ToDoListing l, CancellationToken token = default);
        Task DeleteListing(ToDoListing l, CancellationToken token = default);
    }
    public class EFListingRepository : IListingRepository
    {
        private IdentityContext context;
        public IQueryable<ToDoListing> Listings => context.Listings;

        public EFListingRepository(IdentityContext ctx)
        {
            context = ctx;
        }

        public async Task CreateListing(ToDoListing l, CancellationToken token = default)
        {
            context.Add(l);
            await context.SaveChangesAsync(token);
        }

        public async Task DeleteListing(ToDoListing l, CancellationToken token = default)
        {


              
            context.Remove(l);
            await context.SaveChangesAsync(token);
        }

        public async Task SaveListing(ToDoListing l, CancellationToken token = default)
        {
            await context.SaveChangesAsync(token);
        }
    }
}
