﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ToDoBackend.DataAccess.Models
{
    public interface IToDosRepository
    {
        IQueryable<ToDos> ToDoses { get; }

        Task SaveToDos(ToDos td, CancellationToken token);
        Task CreateToDos(ToDos td, CancellationToken token);
        Task DeleteToDos(ToDos td, CancellationToken token);
    }
    public class EFToDosRepository : IToDosRepository
    {
        private IdentityContext context;
        public IQueryable<ToDos> ToDoses => context.ToDoses;

        public EFToDosRepository(IdentityContext ctx)
        {
            context = ctx;
        }

        public async Task CreateToDos(ToDos td, CancellationToken token)
        {
            context.Add(td);
            await context.SaveChangesAsync(token);
        }

        public async Task DeleteToDos(ToDos td, CancellationToken token)
        {
            context.Remove(td);
            await context.SaveChangesAsync(token);
        }

        public async Task SaveToDos(ToDos td, CancellationToken token)
        {
            await context.SaveChangesAsync(token);
        }
    }
}