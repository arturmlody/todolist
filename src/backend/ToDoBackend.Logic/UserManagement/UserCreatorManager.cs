﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using ToDoBackend.DataAccess.Models;

namespace ToDoBackend.Logic.UserManagement
{
    public interface IUserCreatorManager
    {
        Task<IdentityResult> TryCreate(string username, string password, string name, string surname, string email);
    }
    public class UserCreatorManager : IUserCreatorManager
    {
        private UserManager<User> userManager;

        public UserCreatorManager(UserManager<User> userManager)
        {
            this.userManager = userManager;
        }

        public async Task<IdentityResult> TryCreate(string username, string password, string name, string surname, string email)
        {
            var user = new User { UserName = username, Name = name, Surname = surname, Email = email };
            var result = await userManager.CreateAsync(user, password);
            return result;
        }
    }
}
