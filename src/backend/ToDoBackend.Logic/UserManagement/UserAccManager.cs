﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using ToDoBackend.DataAccess.Models;

namespace ToDoBackend.Logic.UserManagement
{
    public interface IUserAccManager
    {
        Task<string> GetUser (string id);
        Task<bool> ChangePersonalData (string id, string name, string surname);
    }
    public class UserAccManager : IUserAccManager
    {
        private UserManager<User> userManager;

        public UserAccManager(UserManager<User> userManager)
        {
            this.userManager = userManager;
        }

        public async Task<string> GetUser(string id)
        {
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return "User not found.";
            }
            string creds = user.Name + " " + user.Surname;
            return creds;
        } 

        public async Task<bool> ChangePersonalData(string id, string name, string surname)
        {
            var user = await userManager.FindByIdAsync(id);
            if (user == null)
            {
                return false;
            }
            user.Surname = surname;
            user.Name = name;
            await userManager.UpdateAsync(user);
            return true;
        }
    }
}
