﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ToDoBackend.DataAccess.Models;

namespace ToDoBackend.Logic.ToDoList
{
    public interface IToDosManager
    {
        Task<ToDos> Create(long? id, string content, string localization, CancellationToken token = default);
        Task<List<ToDos>> Read(long? listingId, CancellationToken token = default);
        Task<bool> Update(long? id, string content, string localization, CancellationToken token = default);
        Task<bool> Delete(long? id, CancellationToken token = default);
    }
    public class ToDosManager : IToDosManager
    {
        private readonly IToDosRepository repo;
        public ToDosManager(IToDosRepository repo)
        {
            this.repo = repo;
        }

        public async Task<ToDos> Create(long? id, string content, string localization, CancellationToken token = default)
        {
            var toDos = new ToDos
            {
                Content = content,
                Localization = localization,
                ListingId = id
            };
            await repo.CreateToDos(toDos, token);
            return toDos;
        }

        public async Task<bool> Delete(long? id, CancellationToken token = default)
        {

            if (id == null)
            {
                return false;
            }
            var toDos = repo.ToDoses
                .Where(l => l.Id == id)
                .FirstOrDefault();
            if (toDos == null)
            {
                return false;
            }
            await repo.DeleteToDos(toDos, token);
            return true;
        }

        public async Task<List<ToDos>> Read(long? listingId, CancellationToken token = default)
        {
            var result = await repo.ToDoses
                .Where(l => l.ListingId == listingId)
                .AsNoTracking()
                .ToListAsync(token);
            return result;
        }

        public async Task<bool> Update(long? id, string content, string localization, CancellationToken token = default)
        {
            if (id == null)
            {
                return false;
            }
            var toDos = repo.ToDoses
               .Where(l => l.Id == id)
               .FirstOrDefault();
            if (toDos == null)
            {
                return false;
            }
            toDos.Content = content;
            toDos.Localization = localization;
            await repo.SaveToDos(toDos, token);
            return true;
        }
    }
}
