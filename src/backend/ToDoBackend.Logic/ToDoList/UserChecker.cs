﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using ToDoBackend.DataAccess.Models;

namespace ToDoBackend.Logic.ToDoList
{
    public interface IUserChecker
    {
        Task<bool> CheckUser(long? listingId, string userId);
    }
    public class UserChecker : IUserChecker
    {
        private IListingRepository repo;

        public UserChecker( IListingRepository repo)
        {
            this.repo = repo;
        }
        public async Task<bool> CheckUser(long? listingId, string userId) => await repo.Listings.AnyAsync(l => l.Id == listingId && l.UserId == userId);
    }
}
